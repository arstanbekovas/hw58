import React, { Component } from 'react';
import './App.css';
import Modal from "./components/UI/Modal/Modal";
import Button from "./components/UI/Button/Button";
import Alert from "./components/UI/Alert/Alert";

class App extends Component {
    state ={
        modalShow:false,
        dismiss: false
    };

    closed = () => {
        this.setState({modalShow: false})
    };
    openModal = () => {
        this.setState({modalShow:true})
    };
    continued =()=> {
        alert("You pressed continue")
    };
    clickDismissable = () =>{
        this.setState({dismiss:true})
    };

    button = [
        {type: 'primary', label: 'Continue', clicked: this.continued},
        {type: 'danger', label: 'Close', clicked: this.closed}
    ];

  render() {
    return (
      <div className="App">
          <button className="clickMe" onClick={this.openModal}>Click me</button>
          <Modal
              show={this.state.modalShow}
              closed={this.closed}
              title="Some kinda modal title"
          >
              <p>This is modal content</p>
              {this.button.map((button, index) => (
                  <Button
                      key={index}
                      clicked={button.clicked}
                      btnType={button.type}
                      label={button.label}
                  />
              ))}

          </Modal>
          <Alert
              type="warning"
              dismiss={this.clickDismissable}
              visible={this.state.dismiss}
          >This is a warning type alert</Alert>
          <Alert
              type="success"
          >This is a success type alert</Alert>

      </div>
    );
  }
}

export default App;
