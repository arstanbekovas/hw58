import React, {Fragment} from 'react';

const Alert = props => (
    <Fragment>
        <div className={['alert', 'alert-' + props.type].join(' ')}
             role="alert" onClick={props.dismiss} style={{display: !props.visible ? 'block' : 'none'}}>

            {props.children}
            {props.dismiss ? <button type="button" className="close" onClick={props.dismiss}>X</button> : null}
        </div>


    </Fragment>

);

export default Alert;